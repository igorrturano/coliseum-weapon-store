import React from 'react'
import styled from 'styled-components'
import { Container, Row, Col } from 'react-bootstrap'
import {AiFillFacebook, AiFillInstagram} from 'react-icons/ai'


export default () => {
    return (
        <Footer>
        <Container>
            <FooterInfo>
                <Row>
                    <Col md={3}>
                        <div className="title">Sobre Nós</div>
                        <div className="aboutUs">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.
                        </div>   
                    </Col>
                    <Col md={4}>
                    <div className="title">Sobre Nós</div>
                        <div className="bg">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.
                        </div> 
                    </Col>
                    <Col md={2}>
                    <div className="title">Sobre Nós</div>
                        <div className="bg"> 
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.
                        </div> 
                    </Col>
                    <Col md={3}>
                    <div className="title">Sobre Nós</div>
                        <div className="address">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi convallis non lorem in dapibus. Proin et felis fermentum, dapibus ante.
                        </div>   
                    </Col>
                    </Row>
            </FooterInfo>
            <Row>
                <FooterSocial>
                    <AiFillFacebook />
                    <AiFillInstagram />
                </FooterSocial>
                <FooterCopy>
                    All rights reserved
                </FooterCopy>
            </Row>
        </Container>
        </Footer>
    )
}



const Footer = styled.div`
    background: #0F1515;
    padding: 10px 0;
    color: #eee
`

const FooterInfo = styled.div `
    .title{
        font-size: 20px;
        font-weight: 600;
        padding: 10px 0;
        border-bottom: thin solid rgb(99,68,134);
        margin-bottom: 10px;
    }
    color: rgb(99,68,134);
`
const FooterSocial = styled.div`
    cursor: pointer;
    width: 100%;
    border-bottom: 1px dotted #ccc;
    padding: 5px;
    svg {
        margin: 5px;
        font-size: 30px;
        :hover{
            background: rgb(99,68,134);
        }
    }
`

const FooterCopy = styled.div`
    width: 100%
    padding: 10px;
    text-align: center
`

