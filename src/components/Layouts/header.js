import React from 'react'
import { Nav, Navbar, Container, Form } from 'react-bootstrap'
import styled from 'styled-components'
import logo from '../../assets/img/logo.jpg'
import {Link} from 'react-router-dom'

export default () => {

    
    const menu = [
        {
            title: "Home",
            link: '',
            icon: ''
        },
        {
            title: "Sobre",
            link: 'sobre',
            icon: ''
        },
        {
            title: "Produtos",
            link: 'produtos',
            icon: ''
        },
        {
            title: "Serviços",
            link: 'servicos',
            icon: ''
        },
        {
            title: "Contato",
            link: 'contato',
            icon: ''
        }
    ]

    return (
        <Header>
            <Container>
                <Navbar  expand="lg" variant="dark">
                    <Navbar.Brand href="#home">
                    <Logo>
                    <img className="logo" src={logo} alt="Logo" />
                    <span>Açaí do Nosso Jeito</span>
                    </Logo>      
                        </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            {menu.map((item, i) => (
                                <Link to={item.link}>
                                    <Nav.Link key={i} href="#home">{item.title}</Nav.Link>
                                </Link>
                            ))}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </ Container>
        </ Header>
    )
}


const Header = styled.div`
    background-color: rgb(99,68,134);
    .nav-link:hover{
        color: #fac564 !important;
        font-weight: 500;
    }
`

const Logo = styled.div`
font-size: 10px;
font-fontWeight: 600;
margin:0;
font-family: 'Sansita Swashed', cursive;

img {
    width: 100px;
    margin: -5px;
    }

    span {
        color: white;
        margin-left: 15px;
        font-size: 20px;
        text-align: center;
        letter-spacing: 2px;
    }

`