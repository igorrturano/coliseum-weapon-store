import React from 'react'
import styled from 'styled-components'
// import {  } from '../../assets/styles/globalStyle'
import Header from './header'
import Footer from './footer'

const Layout = ({children}) => {
    return(
        <>
        <HeaderContainer>
            <Header />
        </HeaderContainer>
        <Content>
            {children}
        </Content>
        <FooterContainer>
            <Footer />
        </FooterContainer>
        </>
    )
}


const HeaderContainer =styled.div`

`
const Content = styled.div`
    background: pink;
    min-height: 500px;
`

const FooterContainer = styled.div`
    background: green;
    height: 200px;
    wdith: 100%;
`

export default Layout