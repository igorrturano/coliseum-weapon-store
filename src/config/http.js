import axios from 'axios'

const localUrlApi = `http://localhost:3003`


const http = axios.create({
    baseURL: process.ENV.REACT_APP_API || localUrlApi
})

http.defaults.headers['Content-type'] = 'application/json'


export default http;