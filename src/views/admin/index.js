import React from 'react'
import Layout from '../../components/Layouts/index'
import { Route } from 'react-router-dom'
import Dash from './dash'

export default (props) => {
    return(
        <Layout>
            <Route exact basename={props.match.path} path={props.match.path + '/'} component={Dash} />
        </Layout>
        )  
}